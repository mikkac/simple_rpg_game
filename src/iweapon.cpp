#include <iostream>
#include <stdlib.h> //rand()
#include <random>
#include "iitem.hpp"
#include "iweapon.hpp"

IWeapon::IWeapon(std::string name_, double att_, double crit_, double chance_): IItem(name_), Att{att_}, Crit{crit_}, CritChance{chance_} {}

Type IWeapon::GetType(){
    return Type::Weapon;
}

bool IWeapon::IsCriticalHit(){
    //double chance = rand() % 100 + 1;
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(0.0, 100.0);
    if(dis(rd)<=CritChance){
        std::cout << "Critical hit! " << Crit*Att/100.0 << " damage!\n";
    }
}

Hammer::Hammer(std::string name_,double att_, double crit_, double chance_): IWeapon(name_,att_,crit_,chance_) {}

void Hammer::Attack(){
    if(!IsCriticalHit()) std::cout << "Hit! " << Att << " damage!\n";  
}

Dagger::Dagger(std::string name_,double att_, double crit_, double chance_): IWeapon(name_,att_,crit_,chance_) {}


void Dagger::Attack(){
    if(!IsCriticalHit()) std::cout << "Hit! " << Att << " damage!\n";
    if(!IsCriticalHit()) std::cout << "Hit! " << Att << " damage!\n";  
}
