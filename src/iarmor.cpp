#include <iostream>
#include <stdlib.h> //rand()
#include <random>
#include "iitem.hpp"
#include "iarmor.hpp"

IArmor::IArmor(std::string name_, double def_): IItem(name_), Def{def_} {}
double IArmor::GetDefence(){
    //std::cout << "Defence! " << Def << " damage blocked!\n";
    return Def;
}

Helm::Helm(std::string name_, double def_): IArmor(name_,def_) {}
Type Helm::GetType(){
    return Type::Helm;
}

Tunic::Tunic(std::string name_, double def_): IArmor(name_,def_) {}
Type Tunic::GetType(){
    return Type::Tunic;
}

Pants::Pants(std::string name_, double def_): IArmor(name_,def_) {}
Type Pants::GetType(){
    return Type::Pants;
}

Gloves::Gloves(std::string name_, double def_): IArmor(name_,def_) {}
Type Gloves::GetType(){
    return Type::Gloves;
}

Boots::Boots(std::string name_, double def_): IArmor(name_,def_) {}
Type Boots::GetType(){
    return Type::Boots;
}