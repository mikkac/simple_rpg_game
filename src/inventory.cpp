#include <iostream>
#include "inventory.hpp"
#include "iweapon.hpp"
#include "iarmor.hpp"
#include <vector>

void Inventory::ListItems() const{
    for(auto& x : Items){
        std::cout << x << std::endl;
    }
}

void Inventory::DropItem(IItem* item){
    for(auto it = Items.begin(); it != Items.end(); ++it){
        if(item->GetName() == (*it)->GetName()){
           Items.erase(it); 
        }
        else{
            std::cout << "There is no item called " << item->GetName() << " in inventory!\n";
        }
    }
}

void Inventory::AddItem(IItem* item){
    Items.push_back(item);
}

Inventory::~Inventory(){
    Items.clear();
}