#include <iostream>
#include "iitem.hpp"
#include "iweapon.hpp"
#include "iarmor.hpp"
#include "player.hpp"

int main(){
    Hammer ham1("Mlot",100,150,20);
    Dagger dag1("Sztylet",60,150,35);
    Helm hel1("Helm",7);
    Tunic tun1("Tunika",17);
    Pants pan1("Spodnie",10);
    Gloves glo1("Rekawice",3);
    Boots boo1("Buty",5);

    /*std::cout << static_cast<int>(ham1.GetType()) << "\t" << ham1.GetName() << std::endl;
    std::cout << static_cast<int>(dag1.GetType()) << "\t" << dag1.GetName() << std::endl;
    std::cout << static_cast<int>(hel1.GetType()) << "\t" << hel1.GetName() << std::endl;
    std::cout << static_cast<int>(tun1.GetType()) << "\t" << tun1.GetName() << std::endl;
    std::cout << static_cast<int>(pan1.GetType()) << "\t" << pan1.GetName() << std::endl;
    std::cout << static_cast<int>(glo1.GetType()) << "\t" << glo1.GetName() << std::endl;
    std::cout << static_cast<int>(boo1.GetType()) << "\t" << boo1.GetName() << std::endl;
    ham1.Attack();
    dag1.Attack();
    hel1.GetDefence();
    tun1.GetDefence();
    pan1.GetDefence();
    glo1.GetDefence();
    boo1.GetDefence();*/

    Player player1;
    player1.inventory.AddItem(&ham1);
    /* player1.inventory.AddItem(&hel1);
    player1.inventory.AddItem(&tun1);
    player1.inventory.AddItem(&pan1);
    player1.inventory.AddItem(&glo1);
    player1.inventory.AddItem(&boo1); */

    player1.Equip(&ham1); 
    player1.Equip(&hel1); 
    player1.Equip(&tun1); 

    
    std::cout << player1.GetDefence() << std::endl;
    


    return 0;
}