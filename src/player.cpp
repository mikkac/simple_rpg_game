#include "player.hpp"
#include <iostream>

Hammer Player::noweapon = Hammer("Brak",0,0,0);
Helm Player::nohelm = Helm("Brak",0);
Tunic Player::notunic = Tunic("Brak",0);
Pants Player::nopants = Pants("Brak",0);
Gloves Player::nogloves = Gloves("Brak",0);
Boots Player::noboots = Boots("Brak",0);


Player::Player(): Level{1}{
    ActualHP = GetMaxHP();
    weapon = &noweapon;
    helm = &nohelm;
    tunic = &notunic;
    pants = &nopants;
    gloves = &nogloves;
    boots = &noboots;
}

void Player::Equip(IItem* item){
    for(auto it = inventory.Items.begin(); it != inventory.Items.end(); ++it){
        if(item->GetName() == (*it)->GetName()){
            switch(item->GetType()){
                case Type::Weapon:
                    weapon = (IWeapon*)item; break;
                case Type::Helm:
                    helm = (Helm*)item; break;
                case Type::Tunic:
                    tunic = (Tunic*)item; break;
                case Type::Pants:
                    pants = (Pants*)item; break;
                case Type::Gloves:
                    gloves = (Gloves*)item; break;
                case Type::Boots:
                    boots = (Boots*)item; break;
                default:
                    std::cout << "ZLY TYP PODANY DO EQUIP\n";            
            }
            break;
        }
        else{
            std::cout << "There is no item called " << item->GetName() << " in inventory!\n";
        }
    }
}

double Player::GetMaxHP() const{
    return Level*100;
}

double Player::GetActualHP() const{
    return ActualHP;
}

double Player::GetDefence() const{
    double def{0};
    def = helm->GetDefence() + tunic->GetDefence()
        + pants->GetDefence() + gloves->GetDefence()
        + boots->GetDefence(); 
    return def;
}