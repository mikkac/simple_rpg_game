#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "iweapon.hpp"
#include "iarmor.hpp"
#include "inventory.hpp"

class Player{
    double Level;
    double MaxHP;
    double ActualHP;
    
    IWeapon* weapon;
    Helm* helm;
    Tunic* tunic;
    Pants* pants;
    Gloves* gloves;
    Boots* boots;

    static Hammer noweapon; //startowe itemy o zerowych atrybutach (aby uniknac wykonywania operacji na pustym obiekcie)
    static Helm nohelm;
    static Tunic notunic;
    static Pants nopants;
    static Gloves nogloves;
    static Boots noboots;


public:
    Inventory inventory; 
    Player();
    
    void Equip(IItem* item);
    double GetMaxHP() const;
    double GetActualHP() const;
    double GetDefence() const;
    
    
} ;





#endif