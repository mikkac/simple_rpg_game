#ifndef IITEM_HPP
#define IITEM_HPP

#include<string>

enum class Type{Weapon=1, Helm, Tunic, Pants, Gloves, Boots};

class IItem{
protected:
    std::string Name;
public:
    IItem() = default;
    IItem(std::string name_);
    virtual Type GetType()=0;
    std::string GetName() const;
};

#endif