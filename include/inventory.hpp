#ifndef INVENTORY_HPP
#define INVENTORY_HPP

#include "iitem.hpp"
#include <vector>
class Inventory{
public:
    std::vector<IItem*> Items{};
    void ListItems() const;
    void DropItem(IItem* item);
    void AddItem(IItem* item);

    ~Inventory();
};

#endif