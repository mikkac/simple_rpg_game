#ifndef IARMOR_HPP
#define IARMOR_HPP

#include "iitem.hpp"

class IArmor : public IItem{
protected:
    double Def;
public:
    IArmor(std::string name_, double def_);
    double GetDefence();
};

class Helm : public IArmor{
public:
    Helm(std::string name_, double def_);
    virtual Type GetType() override;
};

class Tunic : public IArmor{
public:
    Tunic(std::string name_, double def_);
    virtual Type GetType() override;
};

class Pants : public IArmor{
public:
    Pants(std::string name_, double def_);
    virtual Type GetType() override;
};

class Gloves : public IArmor{
public:
    Gloves(std::string name_, double def_);
    virtual Type GetType() override;
};

class Boots : public IArmor{
public:
    Boots(std::string name_, double def_);
    virtual Type GetType() override;
};


#endif