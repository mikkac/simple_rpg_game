#ifndef IWEAPON_HPP
#define IWEAPON_HPP

#include "iitem.hpp"

class IWeapon : public IItem{
protected:
    double Att;
    double Crit;
    double CritChance;
public:
    IWeapon(std::string name_, double att_, double crit_, double chance_);
    virtual Type GetType() override;
    virtual void Attack()=0;
    bool IsCriticalHit();
};

class Hammer : public IWeapon{
public:
    Hammer(std::string name_, double att_, double crit_, double chance_);
    virtual void Attack() override;
};

class Dagger : public IWeapon{
public:
    Dagger(std::string name_, double att_, double crit_, double chance_);
    virtual void Attack() override;
};


#endif